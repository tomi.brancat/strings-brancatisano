/*
6) El usuario ingresa una palabra. Determinar que
letra aparece mayor cantidad de veces. Para
simplificar el problema, trabaje solo con mayusculas.
*/

#include <stdio.h>

char pal[20];
int letras[20], mayor=0, mayor2=0, flag=0, max=0, i;

int main (){

    printf("Escriba una palabra (M%cximo 20 letras):\n", 160);
    scanf("%s", &pal);

    for (i=0;pal[i]!=NULL;i++){
        for(int x=0;pal[x]!=NULL;x++){
            if(pal[i]==pal[x])letras[i]++;
        }
    }

    for (i=0;pal[i]!=NULL;i++){
        max++;
    }

    for(i=0;i<=max;i++){
        if(letras[i]==1) flag++;
    }

    if(flag==max)
        printf("Todas las letras aparecen la misma cantidad de veces");

    else{
        for(i=1;i<=max;i++){
            if(letras[i]>letras[mayor])
                mayor = i;
            if(letras[i]==letras[mayor] && i!=mayor && pal[i]!=pal[mayor])
                mayor2 = i;
        }


    if(mayor2==0)
        printf("La letra que aparece m%cs veces es: '%c'", 160, pal[mayor]);

    else
        printf("Las letras que aparecen m%cs veces son: '%c' y '%c'", 160, pal[mayor], pal[mayor2]);
    }
}