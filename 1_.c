/*
1) Permitir que el usuario ingrese una palabra de hasta 20 letras. Mostrar en pantalla cuántas letras tiene la palabra ingresada.
Por ejemplo "CASA" debe indicar 4 letras.
*/
#include <stdio.h>

int i, cont;
char pal[21];

int main () {
    printf("Escribir una palabra:\n");
    scanf("%s", &pal);

    for(i=0;i<22;i++){
        if(pal[i]!='\0'){
            cont++;
        }
    }
    printf("La palabra tiene %d letras", cont);
}