/*
3) El usuario ingresa dos strings.
Mostrar en pantalla si son iguales o no, es decir, si tienen las mismas letras en las mismas posiciones.
*/
#include <stdio.h>

int i; 
int flag=0;
char pal1[21], pal2[21];

int main (){
    printf("Escriba la primer palabra:\n");
    scanf("%s", &pal1);
    printf("\nEscriba la segunda palabra:\n");
    scanf("%s", &pal2);

    for(i=0;i<22;i++){
        if(pal1[i]!=pal2[i]){
            flag=1;
            break;
        }
    }

    if(flag==0){
        printf("Las palabras son iguales");
    }
    if(flag==1){
        printf("Las palabras son diferentes");
    }
}