/*
5) El usuario ingresa una palabra. 
Mostrar en pantalla cuántas vocales minúsculas y mayúsculas contiene.
*/

#include <stdio.h>

int i, contm, contM;
char pal[21];

int main (){
    printf("Escriba una palabra:\n");
    scanf("%s", &pal);

    for(i=0;i<22;i++){
        if (pal[i]=='a'|| pal[i]=='e'|| pal[i]=='i'|| pal[i]=='o'|| pal[i]=='u'){
            contm++;
        }
        if (pal[i]=='A'|| pal[i]=='E'|| pal[i]=='I'|| pal[i]=='O'|| pal[i]=='U'){
            contM++;
        }
    }
    printf("\nContiene %d vocales min%csculas y %d vocales may%csculas", contm, 163, contM, 163);
}